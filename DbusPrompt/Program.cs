﻿using System;
using System.Text;
using secrets.DBus;
using Tmds.DBus;

namespace DbusPrompt// Note: actual namespace depends on the project name.
{
    internal class Program
    {
        static void Main(string[] args)
        {
            var systemDbusConnection = Connection.Session;
            var promptService = systemDbusConnection.CreateProxy<IPrompt>("org.freedesktop.Secret.Prompt", "/org/freedesktop/secrets");
            var secretService = systemDbusConnection.CreateProxy<IService>("org.freedesktop.secrets",
                "/org/freedesktop/secrets");
            var session = secretService.OpenSessionAsync("plain", "").Result.result;
            var secretProperties = secretService.GetAllAsync().Result;
            ObjectPath? targetCollection = null;
            string collectionName = "PostSharp";
            foreach (var collection in secretProperties.Collections)
            {
                if (collection.ToString().EndsWith(collectionName))
                {
                    targetCollection = collection;
                    break;
                }
            }

            if (targetCollection == null)
            {
                throw new ArgumentException($"No collection found with the name {collectionName}");
            }

            List<ObjectPath> objectsToUnlock = new List<ObjectPath>();
            objectsToUnlock.Add((ObjectPath)targetCollection);
        
            var collectionProxy = systemDbusConnection.CreateProxy<ICollection>("org.freedesktop.secrets", (ObjectPath)targetCollection);
            var collRes = collectionProxy.GetAllAsync().Result;
            secretService.LockAsync(new ObjectPath[]{(ObjectPath)targetCollection}).GetAwaiter().GetResult();
            var res = secretService.UnlockAsync(new ObjectPath[]{(ObjectPath)targetCollection}).Result;
            Console.WriteLine($"Unlock prompt: ${res.prompt}");
            var prompt = systemDbusConnection.CreateProxy<IPrompt>("org.freedesktop.secrets", res.prompt);

            var pwdTask = prompt.PromptAsync("").ContinueWith((res) =>
            {
                if (res.IsCompletedSuccessfully)
                {
                    Console.WriteLine("Unlocked!");
                    foreach (var collItem in collRes.Items)
                    {
                        var itemProxy = systemDbusConnection.CreateProxy<IItem>("org.freedesktop.secrets", collItem);
                        string name = itemProxy.GetAsync<string>("Label").Result;
                        var secret = itemProxy.GetSecretAsync(session).Result;
                        var str = Encoding.Default.GetString(secret.Item3);
                        Console.WriteLine($"Name: {name} Val: {str}");
                    }

                }
            });
            pwdTask.Wait();
            Console.WriteLine("Finished");

        }
    }
}
